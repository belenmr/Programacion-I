﻿using System;
using System.Collections.Generic;
using ConsoleApplicationLibro;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProjectLibro
{
    [TestClass]
    public class UnitTest1
    {
        private static Libro _Libro;

        [TestInitialize()]
        public void Initialize()
        {
            _Libro = new Libro();
            _Libro.Hojas = new List<Hoja>();
            //Genera las 10 hojas iniciales del libro.
            for (int numero = 0; numero < 10; numero++)
            {
                _Libro.Hojas.Add(new Hoja(numero));
            }        
        }

        [TestMethod]
        public void Test_OrdenarHojasPorNumeroDesc_0()
        {
            List<Hoja> hojas = _Libro.OrdenarHojasPorNumeroDesc();
            Assert.AreEqual(9, hojas[0].Numero);            
        }

        [TestMethod]
        public void Test_OrdenarHojasPorNumeroDesc_1()
        {
            List<Hoja> hojas = _Libro.OrdenarHojasPorNumeroDesc();
            for (int i = 0; i < hojas.Count; i++)
            {                
                Assert.AreEqual(9 - i, hojas[i].Numero);
            }
        }

        [TestMethod]
        public void Test_OrdenarHojasPorNumeroDesc_2()
        {
            _Libro.Hojas[9].Renglones[3].Texto = "hola";
            List<Hoja> hojas = _Libro.OrdenarHojasPorNumeroDesc();
            Assert.AreEqual("hola", hojas[0].Renglones[3].Texto);            
        }

        [TestMethod]
        public void Test_TraerHojaConMasRenglones_0()
        {
            foreach (Hoja hoja in _Libro.Hojas)
            {
                hoja.Renglones.Clear();
            }

            _Libro.Hojas[5].Renglones.Add(new Renglon());
            _Libro.Hojas[5].Renglones.Add(new Renglon());
            _Libro.Hojas[5].Renglones.Add(new Renglon());
            _Libro.Hojas[5].Renglones.Add(new Renglon());

            _Libro.Hojas[9].Renglones.Add(new Renglon());
            _Libro.Hojas[9].Renglones.Add(new Renglon());
            _Libro.Hojas[9].Renglones.Add(new Renglon());
            _Libro.Hojas[9].Renglones.Add(new Renglon());
            _Libro.Hojas[9].Renglones.Add(new Renglon());
           
            Hoja hojaMaxRenglones = _Libro.TraerHojaConMasRenglones();

            Assert.AreEqual(5, hojaMaxRenglones.Renglones.Count);
            Assert.AreEqual(9, hojaMaxRenglones.Numero);
        }

        [TestMethod]
        public void Test_TraerHojaConMasRenglones_1()
        {
            foreach (Hoja hoja in _Libro.Hojas)
            {
                hoja.Renglones.Clear();
            }

            _Libro.Hojas[5].Renglones.Add(new Renglon());
            _Libro.Hojas[5].Renglones.Add(new Renglon());
            _Libro.Hojas[5].Renglones.Add(new Renglon());
            _Libro.Hojas[5].Renglones.Add(new Renglon());

            _Libro.Hojas[9].Renglones.Add(new Renglon());
           
            Hoja hojaMaxRenglones = _Libro.TraerHojaConMasRenglones();

            Assert.AreEqual(4, hojaMaxRenglones.Renglones.Count);
            Assert.AreEqual(5, hojaMaxRenglones.Numero);
        }

        [TestMethod]
        public void Test_EliminarHojasRepetidas_0()
        {
            _Libro.Hojas.Add(new Hoja(9));
            _Libro.Hojas.Add(new Hoja(9));
            _Libro.Hojas.Add(new Hoja(9));

            List<Hoja> hojas = _Libro.EliminarHojasRepetidas();

            Assert.AreEqual(3, hojas.Count);
            Assert.AreEqual(10, _Libro.Hojas.Count);
        }
        
        [TestMethod]
        public void Test_EliminarHojasRepetidas_1()
        {
            _Libro.Hojas.Clear();
            _Libro.Hojas.Add(new Hoja(0));
            _Libro.Hojas.Add(new Hoja(2));
            _Libro.Hojas.Add(new Hoja(2));  //Se debe eliminar
            _Libro.Hojas.Add(new Hoja(2));  //Se debe eliminar
            _Libro.Hojas.Add(new Hoja(1));
            _Libro.Hojas.Add(new Hoja(1));  //Se debe eliminar
            _Libro.Hojas.Add(new Hoja(2));  //Se debe eliminar

            List<Hoja> hojasEliminadas = _Libro.EliminarHojasRepetidas();
            Assert.AreEqual(4, hojasEliminadas.Count);
            Assert.AreEqual(3, _Libro.Hojas.Count);

            #region Sumatoria número de hojas originales.
            int suma = 0;
            foreach (Hoja hoja in _Libro.Hojas)
            {
                suma += hoja.Numero;
            }

            Assert.AreEqual(3, suma);
            #endregion

            #region Sumatoria número de hojas eliminadas.
            int sumaEliminadas = 0;
            foreach (Hoja hoja in hojasEliminadas)
            {
                sumaEliminadas += hoja.Numero;
            }

            Assert.AreEqual(7, sumaEliminadas);
            #endregion
        }

        [TestMethod]
        public void Test_EliminarHojasRepetidas_2()
        {
            _Libro.Hojas.Clear();
            _Libro.Hojas.Add(new Hoja(0));            
            _Libro.Hojas.Add(new Hoja(1));
            _Libro.Hojas.Add(new Hoja(2));

            List<Hoja> hojasEliminadas = _Libro.EliminarHojasRepetidas();
            Assert.AreEqual(0, hojasEliminadas.Count);
            Assert.AreEqual(3, _Libro.Hojas.Count);

            #region Sumatoria número de hojas originales.
            int suma = 0;
            foreach (Hoja hoja in _Libro.Hojas)
            {
                suma += hoja.Numero;
            }

            Assert.AreEqual(3, suma);
            #endregion
        }

        [TestMethod]
        public void Test_Reemplazar_0()
        {
            Hoja hojaConTexto = _Libro.Hojas[9];

            hojaConTexto.Renglones[0].Texto = "Si fui yo y no me arrepiento.";
            hojaConTexto.Renglones[1].Texto = "arrepiento";
            hojaConTexto.Renglones[2].Texto = "no arrepiento";

            List<Hoja> hojas = _Libro.Reemplazar("arrepiento", "asusta");

            Assert.AreEqual(1, hojas.Count);
            Assert.AreEqual("Si fui yo y no me asusta.", hojaConTexto.Renglones[0].Texto);
            Assert.AreEqual("asusta", hojaConTexto.Renglones[1].Texto);
            Assert.AreEqual("no asusta", hojaConTexto.Renglones[2].Texto);
        }
      
        [TestMethod]
        public void Test_Reemplazar_1()
        {           
            List<Hoja> hojas = _Libro.Reemplazar(null, "asusta");

            Assert.AreEqual(0, hojas.Count);            
        }

        [TestMethod]
        public void Test_Reemplazar_2()
        {
            List<Hoja> hojas = _Libro.Reemplazar("", null);

            Assert.AreEqual(0, hojas.Count);
        }

        [TestMethod]
        public void Test_Reemplazar_3()
        {
            List<Hoja> hojas = _Libro.Reemplazar(null, null);

            Assert.AreEqual(0, hojas.Count);
        }

        [TestMethod]
        public void Test_Buscar_0()
        {
            const int nroHoja_1 = 3;
            const int nroHoja_2 = 5;

            _Libro.Hojas[nroHoja_1].Renglones[5].Texto = "en la casa de pinocho";
            _Libro.Hojas[nroHoja_1].Renglones[6].Texto = "pinocho";
            _Libro.Hojas[nroHoja_1].Renglones[7].Texto = "su fue pinocho";

            _Libro.Hojas[nroHoja_2].Renglones[8].Texto = "su fue pinocho";

            List<Hoja> hojas =_Libro.Buscar("pinocho");

            Assert.AreEqual(2, hojas.Count);
            Assert.AreEqual(nroHoja_1, hojas[0].Numero);
            Assert.AreEqual(nroHoja_2, hojas[1].Numero);

            #region Comprobación de NO coloreo
            Assert.AreEqual(false, hojas[0].Renglones[5].Coloreado);
            Assert.AreEqual(false, hojas[0].Renglones[6].Coloreado);
            Assert.AreEqual(false, hojas[0].Renglones[7].Coloreado);

            Assert.AreEqual(false, hojas[1].Renglones[8].Coloreado);
            #endregion
        }

        [TestMethod]
        public void Test_Buscar_1()
        {
            
            List<Hoja> hojas = _Libro.Buscar(null);

            Assert.AreEqual(0, hojas.Count);            
        }
       
        [TestMethod]
        public void Test_Buscar_y_colorear_0()
        {
            const int nroHoja_1 = 3;
            const int nroHoja_2 = 5;

            _Libro.Hojas[nroHoja_1].Renglones[5].Texto = "en la casa de pinocho";
            _Libro.Hojas[nroHoja_1].Renglones[6].Texto = "pinocho";
            _Libro.Hojas[nroHoja_1].Renglones[7].Texto = "su fue pinocho";

            _Libro.Hojas[nroHoja_2].Renglones[8].Texto = "su fue pinocho";

            List<Hoja> hojas = _Libro.Buscar("pinocho", true);

            Assert.AreEqual(2, hojas.Count);
            Assert.AreEqual(nroHoja_1, hojas[0].Numero);
            Assert.AreEqual(nroHoja_2, hojas[1].Numero);

            #region Comprobación de coloreo
            Assert.AreEqual(false, hojas[0].Renglones[4].Coloreado);
            Assert.AreEqual(true, hojas[0].Renglones[5].Coloreado);
            Assert.AreEqual(true, hojas[0].Renglones[6].Coloreado);
            Assert.AreEqual(true, hojas[0].Renglones[7].Coloreado);
            Assert.AreEqual(false, hojas[0].Renglones[8].Coloreado);

            Assert.AreEqual(true, hojas[1].Renglones[8].Coloreado);
            #endregion
        }

        [TestMethod]
        public void Test_Buscar_y_colorear_1()
        {
            List<Hoja> hojas = _Libro.Buscar(null, true);

            Assert.AreEqual(0, hojas.Count);            
        }

    }
}
