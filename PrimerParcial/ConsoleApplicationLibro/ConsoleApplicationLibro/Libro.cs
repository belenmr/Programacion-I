﻿/*****************************************************************
 * Apellido, nombre: _____________________________________________
 *****************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplicationLibro
{
    /// <summary>
    /// Representación en objetos de un libro.
    /// </summary>
    public class Libro
    {
        public Libro()
        {
            //Genera las 10 hojas del libro.
            for (int numero = 0; numero < 10; numero++)
            {
                this.Hojas.Add(new Hoja(numero));
            }
        }

        /// <summary>
        /// Titulo del libro.
        /// </summary>
        public string Titulo { set; get; }
        /// <summary>
        /// Hojas del libro.
        /// </summary>
        public List<Hoja> Hojas = new List<Hoja>();

        /// <summary>
        /// Busca aquellas hojas del libro que contengan el texto indicado.
        /// </summary>
        /// <param name="buscado">
        /// Texto a buscar.
        /// </param>
        /// <remarks>
        /// Si el parámetro vale nulo (null) el método debe 
        /// retornar un una lista con cero ítems.
        /// </remarks>
        /// <returns>
        /// Lista de hojas que contienen el texto buscado, en caso de 
        /// no encontrarlo debe retornar una lista con cero ítems, 
        /// nunca retorna null.
        /// </returns>
        public List<Hoja> Buscar(string buscado)
        {

            List<Hoja> hojasConTextoIndicado = new List<Hoja>();

            if (buscado != null)
            {
                foreach (Hoja hoja in this.Hojas)
                {
                    foreach (Renglon renglon in hoja.Renglones)
                    {
                        if (renglon.Texto.Contains(buscado))
                        {
                            if (!hojasConTextoIndicado.Contains(hoja))
                            {
                                hojasConTextoIndicado.Add(hoja);
                            }
                        }
                    }
                }
            }

            return hojasConTextoIndicado;
        }


        /// <summary>
        /// Busca aquellas hojas que contengan el texto indicado 
        /// y colorea los renglones que contienen el texto.
        /// </summary>
        /// <param name="buscado">
        /// Texto a buscar.
        /// </param>       
        /// <param name="colorear">
        /// Indica si desea colorear el renglón que contiene el texto encontrado.
        /// </param>
        /// <remarks>
        /// Si el parámetro vale nulo (null) el método debe 
        /// retornar un una lista con cero ítems.
        /// </remarks>
        /// <returns>
        /// Lista de hojas que contienen el texto buscado, en caso de 
        /// no encontrarlo debe retornar una lista con cero ítems, 
        /// nunca retorna null.
        /// </returns>
        public List<Hoja> Buscar(string buscado, bool colorear)
        {
            List<Hoja> hojasConTextoIndicado = new List<Hoja>();

            if (buscado != null)
            {
                foreach (Hoja hoja in this.Hojas)
                {
                    foreach (Renglon renglon in hoja.Renglones)
                    {
                        if (renglon.Texto.Contains(buscado))
                        {
                            renglon.Coloreado = colorear;
                            if (!hojasConTextoIndicado.Contains(hoja))
                            {
                                hojasConTextoIndicado.Add(hoja);
                            }
                        }
                    }
                }
            }

            return hojasConTextoIndicado;
        }


        /// <summary>
        /// Reemplaza un texto por otro en cada una de las hojas del libro.
        /// </summary>
        /// <param name="buscado">
        /// Texto a buscar.
        /// </param>
        /// <param name="reemplazo">
        /// Texto de reemplazo.
        /// </param>
        /// <returns>
        /// Lista de hojas que contienen el texto buscado. 
        /// En caso de no encontrarlo debe retornar una lista con cero ítems, 
        /// nunca retorna null.
        /// Si alguno de los parámetros es nulo debe retornar una lista con cero ítems.
        /// </returns>
        public List<Hoja> Reemplazar(string buscado, string reemplazo)
        {
            List<Hoja> hojasConTextoReemplazado = new List<Hoja>();

            if (buscado != null && reemplazo!=null)
            {
                foreach (Hoja hoja in this.Hojas)
                {
                    foreach (Renglon renglon in hoja.Renglones)
                    {
                        if (renglon.Texto.Contains(buscado))
                        {
                            renglon.Texto = renglon.Texto.Replace(buscado, reemplazo); 

                            if (!hojasConTextoReemplazado.Contains(hoja))
                            {
                                hojasConTextoReemplazado.Add(hoja);
                            }
                        }
                    }
                }
            }

            return hojasConTextoReemplazado; //retorno hoja con texto buscado o reemplazado?
        }

        /// <summary>
        /// Retorna la hoja del libro que posea la mayor cantidad de renglones.
        /// </summary>
        /// <returns></returns>
        public Hoja TraerHojaConMasRenglones()
        {
            Hoja hojaConMasRenglones = this.Hojas[0]; //suponiendo que es la hoja con menos renglones, como para tomar de referencia

            foreach (Hoja hoja in this.Hojas)
            {
                if (hoja.Renglones.Count > hojaConMasRenglones.Renglones.Count)
                {
                    hojaConMasRenglones = hoja;
                }
            }

            return hojaConMasRenglones;
        }

        /// <summary>
        /// Ordena por el algoritmo de la BURBUJA
        /// las hojas del libro en forma descendente, 
        /// en otras palabras de la hoja con mayor número de hoja a 
        /// la de menor número.
        /// </summary>
        /// <returns>
        /// La lista de hojas ordenadas.
        /// </returns>
        public List<Hoja> OrdenarHojasPorNumeroDesc()
        {
            bool enroque;

            do
            {
                enroque = false;
                for (int i = 0; i < this.Hojas.Count - 1; i++)
                {
                    if (this.Hojas[i].Numero < this.Hojas[i + 1].Numero)
                    {
                        enroque = true;

                        Hoja aux = this.Hojas[i];
                        this.Hojas[i] = this.Hojas[i + 1];
                        this.Hojas[i + 1] = aux;
                    }

                }

            } while (enroque);

            return this.Hojas;
        }       
        
        /// <summary>
        /// Elimina las hojas que están repetidas.
        /// Dos hojas están repetidas cuando poseen el mismo número de hoja,
        /// en otras palabras elimina las copias y deja la original.
        /// Por ejemplo si hay 3 hojas repartidas solo deja una, 
        /// eliminando las 2 copias.
        /// </summary>
        /// <returns>
        /// Retorna la lista de hojas que fueron eliminadas. 
        /// En caso de no encontrar hojas repetidas, debe retornar una lista con cero ítems, 
        /// nunca retorna null.
        /// </returns>
        public List<Hoja> EliminarHojasRepetidas()
        {
            List<Hoja> hojasEliminadas = new List<Hoja>();
            for (int i = 0; i < Hojas.Count; i++)
            {
                Hoja hoja = this.Hojas[i];
                for (int j = 0; j < Hojas.Count ; j++)
                {
                    if (this.Hojas[i].Numero == this.Hojas[j].Numero && i != j)
                    {
                        hojasEliminadas.Add(this.Hojas[j]);
                        this.Hojas.Remove(this.Hojas[j]);
                    }
                }
            }

            return hojasEliminadas;
        }
    }
}
