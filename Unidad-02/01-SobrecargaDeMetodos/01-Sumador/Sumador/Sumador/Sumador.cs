﻿using System;

namespace Sumador
{
    /// <summary>
    /// Responsable de realizar adiciones diversas
    /// </summary>
    class Sumador
    {
        /// <summary>
        /// Suma de dos numeros enteros
        /// </summary>
        /// <param name="x1">Sumando 1</param>
        /// <param name="x2">Sumando 2</param>
        /// <returns></returns>
        public int Sumar(int x1, int x2)
        {
            return x1 + x2;
        }

        /// <summary>
        /// Suma de dos cadenas de caracteres, concatenandolas
        /// </summary>
        /// <param name="s1">Primera cadena de caracteres</param>
        /// <param name="s2">Segunda cadena de caracteres</param>
        /// <returns></returns>
        public string Sumar(string s1, string s2)
        {
            return s1 + s2;
        }
    }
}
