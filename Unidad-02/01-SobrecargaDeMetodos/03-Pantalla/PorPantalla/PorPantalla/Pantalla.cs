﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PorPantalla
{
    class Pantalla
    {
        public void Mostrar(string mensaje)
        {
            Console.WriteLine(mensaje);
        }

        public void Mostrar(string mensaje, int columna, int fila)
        {

        }

        public void Mostrar(string mensaje, int columna, int fila, ConsoleColor colorLetra)
        {

        }

        public void Mostrar(string mensaje, int columna, int fila, ConsoleColor colorLetra, ConsoleColor colorFondo)
        {

        }
    }
}
