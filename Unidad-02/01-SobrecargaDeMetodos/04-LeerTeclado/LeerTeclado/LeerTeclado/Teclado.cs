﻿using System;

namespace LeerTeclado
{
    /// <summary>
    /// Representacion en objeto de un teclado de computadora.
    /// </summary>
    class Teclado
    {
        public void Leer(out int valor)
        {
            valor = int.Parse(Console.ReadLine());
        }

        public void Leer(out float valor)
        {
            valor = float.Parse(Console.ReadLine());
        }

        public void Leer(out char valor)
        {
            valor = char.Parse(Console.ReadLine());
        }

        public void Leer(out bool valor)
        {
            valor = bool.Parse(Console.ReadLine());
        }
    }
}
