﻿using System;

namespace Sumador
{
    class Program
    {
        static void Main(string[] args)
        {
        	Sumador sumador = new Sumador();

            //Suma algebraica de dos numeros
            int sumaNumerica = sumador.Sumar(10, 1);

            //Concatenacion de cadenas de caracteres
            string sumaCaracteres = sumador.Sumar("Manuel Sadosky", ", padre de la computacion en Argentina");

            //Suma dos cuadernos dando como resultado uno con la suma de sus hojas
            Cuaderno cuadernoA = new Cuaderno();
            Cuaderno cuadernoB = new Cuaderno();
            cuadernoA.CantidadDeHojas = 100;
            cuadernoB.CantidadDeHojas = 201;
            Cuaderno cuadernoResultante = sumador.Sumar(cuadernoA,cuadernoB);

            Console.WriteLine(cuadernoResultante.CantidadDeHojas);

        }
    }
}
