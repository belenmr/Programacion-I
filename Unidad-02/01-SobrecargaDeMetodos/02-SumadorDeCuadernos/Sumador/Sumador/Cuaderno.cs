﻿using System;

namespace Sumador
{
    /// <summary>
    /// Representacion en objetos de un cuaderno
    /// </summary>
    class Cuaderno
    {
        public int CantidadDeHojas { set; get; }
    }
}
