﻿using System;

namespace Punto
{
    /// <summary>
    /// Representacion del objeto punto
    /// </summary>
    class Punto
    {
        private int _X;
        private int _Y;

        /// <summary>
        /// Constructor de un objeto punto a partir de coordenadas
        /// </summary>
        /// <param name="x">Coordenada en el eje X</param>
        /// <param name="y">Coordenada en el eje Y</param>
        public Punto(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Constructor de un objeto punto a partir de coordenadas y un color
        /// </summary>
        /// <param name="x">Coordenada en el eje X</param>
        /// <param name="y">Coordenada en el eje Y</param>
        /// <param name="colorPunto">Color del punto</param>
        public Punto(int x, int y, ConsoleColor colorPunto) : this(x, y)
        {
            Console.ForegroundColor = colorPunto;
        }

        /// <summary>
        /// Constructor de un objeto punto a partir de coordenadas, color del punto y color de fondo
        /// </summary>
        /// <param name="x">Coordenada en el eje X</param>
        /// <param name="y">Coordenada en el eje Y</param>
        /// <param name="colorPunto">Color del punto</param>
        public Punto(int x, int y, ConsoleColor colorPunto, ConsoleColor colorFondo) : this(x, y, colorPunto)
        {
            Console.BackgroundColor = colorFondo;
        }

        /// <summary>
        /// Valor de la coordenada abscisa del punto
        /// </summary>
        public int X
        {
            set
            {
                if (value >= 0)
                {
                    _X = value;
                }
            }

            get
            {
                return _X;
            }
        }

        /// <summary>
        /// Valor de la coordenada ordenada del punto
        /// </summary>
        public int Y
        {
            set
            {
                if (value >= 0)
                {
                    _Y = value;
                }
            }

            get
            {
                return _Y;
            }
        }

        /// <summary>
        /// Muestra en pantalla el punto
        /// </summary>
        public void Imprimir()
        {
            Console.SetCursorPosition(this.X, this.Y);
            Console.WriteLine(".");
        }
    }
}
