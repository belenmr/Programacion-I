﻿using System;

namespace Punto
{
    class Program
    {
        static void Main(string[] args)
        {
            Punto punto = new Punto(10, 15);
            punto.Imprimir();

            Punto puntoRojo = new Punto(20, 5, ConsoleColor.Red);
            puntoRojo.Imprimir();

            Punto puntoAmMag = new Punto(5, 25, ConsoleColor.Yellow, ConsoleColor.Magenta);
            puntoAmMag.Imprimir();
        }
    }
}
