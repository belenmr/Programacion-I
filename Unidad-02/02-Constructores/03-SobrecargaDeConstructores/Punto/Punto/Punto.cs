﻿using System;

namespace Punto
{
    /// <summary>
    /// Representacion del objeto punto
    /// </summary>
    class Punto
    {
        private int _X;
        private int _Y;

        /// <summary>
        /// Constructor de un objeto punto a partir de sus coordenadas
        /// </summary>
        /// <param name="x">Coordenada en el eje X</param>
        /// <param name="y">Coordenada en el eje Y</param>
        public Punto(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Constructor de un objeto punto a partir de otro punto
        /// </summary>
        /// <param name="punto"></param>
        public Punto(Punto punto) : this(punto.X + 1, punto.Y + 1)
        {

        }

        /// <summary>
        /// Valor de la coordenada abscisa del punto
        /// </summary>
        public int X
        {
            set
            {
                if (value >= 0)
                {
                    _X = value;
                }
            }

            get
            {
                return _X;
            }
        }

        /// <summary>
        /// Valor de la coordenada ordenada del punto
        /// </summary>
        public int Y
        {
            set
            {
                if (value >= 0)
                {
                    _Y = value;
                }
            }

            get
            {
                return _Y;
            }
        }

        /// <summary>
        /// Muestra en pantalla el punto
        /// </summary>
        public void Imprimir()
        {
            Console.SetCursorPosition(this.X, this.Y);
            Console.WriteLine(".");
        }
    }
}
