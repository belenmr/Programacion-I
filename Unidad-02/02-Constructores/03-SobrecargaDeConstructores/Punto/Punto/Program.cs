﻿using System;

namespace Punto
{
    class Program
    {
        static void Main(string[] args)
        {
            Punto puntoOrigen = new Punto(20, 3);
            puntoOrigen.Imprimir();

            //Punto puntoDesplazado = new Punto(puntoOrigen);
            Punto puntoDesplazado = puntoOrigen;

            for (int i = 0; i < 7; i++)
            {
                puntoDesplazado = new Punto(puntoDesplazado);
                puntoDesplazado.Imprimir();
            }
            Console.WriteLine();
        }
    }
}
