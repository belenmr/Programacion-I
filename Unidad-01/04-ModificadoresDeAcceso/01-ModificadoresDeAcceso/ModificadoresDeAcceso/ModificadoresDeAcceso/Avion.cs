﻿namespace ModificadoresDeAcceso
{
    /// <summary>
    /// Representación de un avión en objetos.
    /// </summary>
    public class Avion
    {
        private Motor _Motor = new Motor();
        /// <summary>
        /// Aumenta la velocidad del avión
        /// </summary>
        public void Acelerar()
        {
            _Motor.RPM++;
        }

        /// <summary>
        /// Disminuye la velocidad del avión
        /// </summary>
        public void Disminuir()
        {
            _Motor.RPM--;
        }

        /// <summary>
        /// Motor del avion.
        /// </summary>
        private class Motor
        {
            private int _RPM;

            /// <summary>
            /// Revoluciones Por Minuto (RPM)
            /// </summary>
            public int RPM
            {
                set
                {
                    if (value > 0)
                    {
                        _RPM = value;
                    }
                }

                get
                {
                    return _RPM;
                }
            }

        }
    }
}
