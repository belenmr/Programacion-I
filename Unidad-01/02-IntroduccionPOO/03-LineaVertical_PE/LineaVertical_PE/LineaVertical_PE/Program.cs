﻿using System;

namespace LineaVertical_PE
{
    class Program
    {
        static void Main(string[] args)
        {
            int longitudLinea = 10;
            string caracterImpreso = "|";

            for (int i = 0; i < longitudLinea; i++)
            {
                Console.WriteLine(caracterImpreso);
            }
        }
    }
}
