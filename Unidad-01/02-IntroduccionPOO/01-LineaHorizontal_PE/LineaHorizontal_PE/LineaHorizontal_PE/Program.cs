﻿using System;

namespace LineaHorizontal_PE
{
    class Program
    {
        static void Main(string[] args)
        {
            int longitudLinea = 10;
            string caracterImpreso = "_";

            for (int i = 0; i < longitudLinea; i++)
            {
                Console.Write(caracterImpreso);
            }

            Console.WriteLine();
        }
    }
}
