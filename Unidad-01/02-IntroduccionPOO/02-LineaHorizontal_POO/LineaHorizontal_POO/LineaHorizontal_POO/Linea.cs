﻿using System;

namespace LineaHorizontal_POO
{
    class Linea
    {
        private int _Longitud;
        public int Longitud
        {
            set
            {
                if (value > 0)
                    _Longitud = value;
                else
                    throw new Exception("Largo no permitido");
            }

            get
            {
                return _Longitud;
            }
        }

        /// <summary>
        /// Imprime una línea horizontal
        /// </summary>
        public void Imprimir()
        {
            for (int i = 0; i < this._Longitud; i++)
            {
                Console.Write("_");
            }
            Console.WriteLine();
        }
    }
}
