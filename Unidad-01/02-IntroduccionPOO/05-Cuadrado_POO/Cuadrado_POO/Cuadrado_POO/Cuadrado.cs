﻿using System;

namespace Cuadrado_POO
{
    class Cuadrado
    {
        private int _LongitudLado;
        public int LongitudLado
        {
            set
            {
                if (value > 0)
                    _LongitudLado = value;
                else
                    throw new Exception("Medida no permitida");
            }

            get { return _LongitudLado; }
        }

        /// <summary>
        /// Imprime el cuadrado
        /// </summary>
        public void Imprimir()
        {
            this.ImprimirBase();

            this.ImprimirLaterales();

            if (this._LongitudLado != 1)
            {
                this.ImprimirBase();
            }
        }

        /// <summary>
        /// Imprime los caracteres que representan la base del cuadrado
        /// </summary>
        private void ImprimirBase()
        {
            for (int i = 0; i < this._LongitudLado; i++)
            {
                Console.Write("*");
            }

            Console.WriteLine();
        }

        /// <summary>
        /// Imprime los caracteres que representan los laterales del cuadrado
        /// </summary>
        private void ImprimirLaterales()
        {
            for (int i = 0; i < this._LongitudLado - 2; i++)
            {
                Console.Write("*");

                for (int j = 0; j < this._LongitudLado - 2; j++)
                {
                    Console.Write(" ");
                }

                Console.Write("*");
                Console.WriteLine("");
            }
        }
    }
}
