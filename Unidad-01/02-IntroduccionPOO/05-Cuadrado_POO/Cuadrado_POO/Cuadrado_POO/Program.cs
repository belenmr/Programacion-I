﻿using System;

namespace Cuadrado_POO
{
    class Program
    {
        static void Main(string[] args)
        {
            Cuadrado miCuadrado = new Cuadrado();
            miCuadrado.LongitudLado = 5;
            miCuadrado.Imprimir();

            Console.ReadKey();
        }
    }
}
