﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication
{
    interface IPersistidor
    {
        void Delete(int idProducto);
        List<Producto> FilterByMarca(string marca);
        List<Producto> GetAll();
        Producto GetById(int idProducto);
        bool Insert(Producto producto);
        bool Save(Producto producto);
        void SetProductoID(Producto producto);
        bool Update(Producto producto);

    }
}
