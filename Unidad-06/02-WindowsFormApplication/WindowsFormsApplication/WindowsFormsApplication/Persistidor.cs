﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication
{
    /// <summary>
    /// Guarda y recupera la información hacia y desde la base de datos.
    /// </summary>
    class Persistidor : IPersistidor
    {
        /// <summary>
        /// Esta lista hace de base de datos en la cual 
        /// persistiremos los productos de nuestra aplicación.
        /// </summary>
        private static List<Producto> _ProductoDataBase = new List<Producto>();
        public void Delete(int idProducto)
        {
            foreach (Producto producto in _ProductoDataBase)
            {
                if (producto.Id == idProducto)
                {
                    _ProductoDataBase.Remove(producto);
                }
            }
        }

        public List<Producto> FilterByMarca(string marca)
        {
            List<Producto> ProductosDeLaMarca = new List<Producto>();

            foreach (Producto producto in _ProductoDataBase)
            {
                if (producto.Marca.Contains(marca))
                {
                    ProductosDeLaMarca.Add(producto);
                }
            }

            return ProductosDeLaMarca;
        }

        public List<Producto> GetAll()
        {
            return _ProductoDataBase;
        }

        /// <summary>
        /// Devuelve un producto
        /// </summary>
        /// <param name="idProducto">ID del producto a buscar</param>
        /// <returns>Un registro con los valores del Producto</returns>
        public Producto GetById(int idProducto)
        {
            Producto resultadoProducto = null;

            foreach (Producto producto in _ProductoDataBase)
            {
                if (producto.Id == idProducto)
                {
                    resultadoProducto = producto;
                }
            }

            return resultadoProducto;
        }

        /// <summary>
        /// Inserta un nuevo Producto en la lista
        /// </summary>
        /// <remarks>
        /// Primero y siguiendo el orden de las acciones CRUD
        /// Crearemos un Método que se encarga de insertar un nuevo Producto es nuestra tabla Producto
        /// </remarks>
        /// <param name="producto">Entidad contenedora de los valores a insertar</param>
        public bool Insert(Producto producto)
        {
            _ProductoDataBase.Add(producto);
            bool insertado = true;
            this.SetProductoID(producto);

            return insertado;
        }

        /// <summary>
        /// Persite un producto en la base de datos.
        /// </summary>
        /// <param name="producto"></param>
        public bool Save(Producto producto)
        {
            bool guardado = false;

            foreach (Producto productoAux in _ProductoDataBase)
            {
                if (productoAux.Id.Value == producto.Id.Value)
                {
                    guardado = this.Update(producto);
                }
                
            }

            if (guardado == false)
            {
                guardado = this.Insert(producto);
            }

            return guardado;
        }

        /// <summary>
        /// Establece el ID de un producto.
        /// </summary>
        public void SetProductoID(Producto producto)
        {
            int maxId = 0; // los ID arrancan en 0

            //busco el maximo ID de la lista
            foreach (Producto productoAux in _ProductoDataBase)
            {
                if (productoAux.Id.HasValue == true)
                {
                    if (productoAux.Id > maxId)
                    {
                        maxId = productoAux.Id.Value; // Obtengo el maximo ID usado
                    }
                }
            }
            producto.Id = maxId + 1;
        }

        /// <summary>
        /// Actualiza el Producto proporcionado
        /// </summary>
        /// <param name="producto">Valores utilizados para hacer el Update al registro</param>
        public bool Update(Producto producto)
        {
            bool actualizado;
            int i = 0;
            const int NO_EXISTE = -1;
            int index = NO_EXISTE;
            foreach (Producto productoAux in _ProductoDataBase)
            {
                if (productoAux.Id.Value == producto.Id.Value)
                {
                    index = i;
                }
                i++;
            }

            if (!(index == NO_EXISTE))
            {
                _ProductoDataBase[index] = producto;
                actualizado = true;
            }
            else
            {
                actualizado = false;
            }

            return actualizado;
        }
    }
}
