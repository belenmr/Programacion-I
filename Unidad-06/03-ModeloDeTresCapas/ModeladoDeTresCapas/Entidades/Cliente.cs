﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades
{
    public class Cliente
    {
        public int? ID { set; get; }
        public int DNI { set; get; }
        public string NombreYApellido { set; get; }
        public int Deuda { set; get; }
    }
}
