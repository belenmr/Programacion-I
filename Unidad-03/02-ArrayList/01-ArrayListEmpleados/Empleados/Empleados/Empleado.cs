﻿using System;

namespace Empleados
{
    /// <summary>
    /// Representación en objetos de un empleado.
    /// </summary>
    class Empleado
    {
        private int _Sueldo;
        private string _Nombre;

        /// <summary>
        /// Nombre del empleado.
        /// </summary>
        public string Nombre
        {
            set
            {
                if (value.Length > 0 )
                {
                    _Nombre = value;
                }
            }
            get { return _Nombre; }
        }
        
        /// <summary>
        /// Sueldo del empleado.
        /// </summary>
        public int Sueldo
        {
            set
            {
                if (value > 0)
                {
                    _Sueldo = value;
                }
            }
            get { return _Sueldo; }
        }
    }
}
