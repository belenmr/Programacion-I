﻿using System;
using System.Collections.Generic;

namespace ABMUtilizandoForeach
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            //program.AltaConForeach();

            program.BajaConForeach();



        }

        private void AltaConForeach()
        {
            List<string> textos = new List<string>();
            textos.Add("hola mundo");
            textos.Add("le dijo hola");
            textos.Add("gracias totales");
            textos.Add("hola, soy un ladron");

            List<string> textosAgregados = new List<string>();

            foreach (string texto in textos)
            {
                if (texto.Contains("hola"))
                {
                    textosAgregados.Add(texto.Replace("hola", "chau"));
                }
            }

            textos.AddRange(textosAgregados);

            //ImprimirTexto(textos);
        }

        private void BajaConForeach()
        {
            List<string> textos = new List<string>();
            textos.Add("hola mundo");
            textos.Add("le dijo hola");
            textos.Add("gracias totales");
            textos.Add("hola, soy un ladron");

            List<string> textosAEliminar = new List<string>();

            foreach (string texto in textos)
            {
                if (texto.Contains("hola"))
                {
                    textosAEliminar.Add(texto);
                }
            }

            foreach (string texto in textosAEliminar)
            {
                textos.Remove(texto);
            }

            //ImprimirTexto(textos);
        }



        /// <summary>
        /// Imprime una lista de tipo string
        /// </summary>
        /// <param name="textos"></param>
        private void ImprimirTexto(List<string> textos)
        {
            foreach (string texto in textos)
            {
                Console.WriteLine(texto);
            }
        }
    }
}
