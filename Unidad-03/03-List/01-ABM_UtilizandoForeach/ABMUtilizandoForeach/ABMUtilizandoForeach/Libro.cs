﻿namespace ABMUtilizandoForeach
{
    class Libro
    {
        public Libro(string texto)
        {
            this.Texto = texto;
        }

        public string Texto { set; get; }
    }
}
