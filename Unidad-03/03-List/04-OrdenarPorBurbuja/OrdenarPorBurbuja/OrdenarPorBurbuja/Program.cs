﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdenarPorBurbuja
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numeros = new List<int>();

            for (int i = 0; i<10; i++)
            {
                Console.WriteLine("Ingrese un numero a la lista");
                numeros.Add(int.Parse(Console.ReadLine()));
            }

            Console.WriteLine("Lista desordenada: ");
            ImprimirLista(numeros);

            OrdenarPorBurbuja(numeros);
            Console.WriteLine("Lista ordenada: ");
            ImprimirLista(numeros);

        }

        private static void OrdenarPorBurbuja(List<int> listaNumeros)
        {
            bool enroque;
            do
            {
                enroque = false;
                for (int i = 0; i < listaNumeros.Count-1; i++)
                {
                    if (listaNumeros[i] > listaNumeros[i+1])
                    {
                        enroque = true;

                        int aux = listaNumeros[i];
                        listaNumeros[i] = listaNumeros[i + 1];
                        listaNumeros[i + 1] = aux;
                    }
                
                }

            } while (enroque);
        }

        private static void ImprimirLista(List<int> numeros)
        {
            foreach (var numero in numeros)
            {
                Console.WriteLine(numero);
            }
        }
    }
}
