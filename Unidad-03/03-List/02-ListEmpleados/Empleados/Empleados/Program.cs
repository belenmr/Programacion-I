﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empleados
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Empleado> empleados = new List<Empleado>();

            ConsoleKeyInfo teclaInput;

            bool salir = false;

            do
            {
                teclaInput = IngresoMenu();

                switch (teclaInput.Key)
                {
                    case ConsoleKey.Add:
                        AdicionarEmpleado(empleados);
                        break;
                    case ConsoleKey.Subtract:
                        EliminarEmpleado(empleados);
                        break;
                    case ConsoleKey.Enter: //no la reconoce
                        ListarEmpleados(empleados);
                        break;
                    case ConsoleKey.Escape:
                        salir = true;
                        break;
                }
            } while (!salir);
        }

        private static ConsoleKeyInfo IngresoMenu()
        {
            Console.Clear();
            Console.WriteLine("Tecla + : Adicionar un nuevo empleado.");
            Console.WriteLine("Tecla - : Eliminar un empleado.");
            Console.WriteLine("Tecla Enter : Listar empleados.");
            Console.WriteLine("Tecla Esc : Salir.");
            ConsoleKeyInfo teclaIngresada = Console.ReadKey();

            return teclaIngresada;
        }

        private static void AdicionarEmpleado(List<Empleado> empleados)
        {
            Console.Clear();

            Empleado nuevoEmpleado = new Empleado();
            Console.Write("Nombre: ");
            nuevoEmpleado.Nombre = Console.ReadLine();
            Console.Write("Sueldo: ");
            nuevoEmpleado.Sueldo = int.Parse(Console.ReadLine());

            empleados.Add(nuevoEmpleado);
        }

        private static void EliminarEmpleado(List<Empleado> empleados)
        {
            Console.Clear();

            Empleado empleadoAEliminar = new Empleado();
            Console.Write("Nombre del empleado a eliminar: ");
            empleadoAEliminar.Nombre = Console.ReadLine();

            foreach (Empleado empleado in empleados)
            {
                if (empleadoAEliminar.Nombre.ToUpper().Equals(empleado.Nombre.ToUpper()))
                {
                    empleadoAEliminar = empleado;
                }
            }

            if (empleadoAEliminar != null)
            {
                empleados.Remove(empleadoAEliminar);
            }
            else
            {
                Console.WriteLine("{0} no encontrado en la lista de empleados", empleadoAEliminar.Nombre);
            }

        }

        private static void ListarEmpleados(List<Empleado> empleados)
        {
            Console.Clear();
            Console.WriteLine("Nombre: \t\t\t Sueldo:");
            if (empleados.Count != 0)
            {
                foreach (Empleado empleado in empleados)
                {
                    Console.WriteLine("{0} \t\t\t {1}", empleado.Nombre, empleado.Sueldo);
                }
            }
            else
            {
                Console.WriteLine("La lista de empleados esta vacía.");
            }
        }

    
    }
}
