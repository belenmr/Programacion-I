﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empleados
{
    class Empleado
    {
        private string _Nombre;
        private decimal _Sueldo;

        /// <summary>
        /// Nombre del empleado
        /// </summary>
        public string Nombre { set; get; }

        /// <summary>
        /// Sueldo del empleado
        /// </summary>
        public decimal Sueldo
        {
            set
            {
                if (value > 0)
                    _Sueldo = value;
                else
                    _Sueldo = 0;
            }

            get { return _Sueldo; }
        }
    }
}
