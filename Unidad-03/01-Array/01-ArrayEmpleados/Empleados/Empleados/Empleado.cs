﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empleados
{
    /// <summary>
    /// Representación en objetos de un empleado.
    /// </summary>
    public class Empleado
    {
        private int _Sueldo;

        /// <summary>
        /// Nombre del empleado.
        /// </summary>
        public String Nombre { set; get; }

        /// <summary>
        /// Sueldo del empleado.
        /// </summary>
        public int Sueldo
        {
            set
            {
                if (value > 0)
                {
                    _Sueldo = value;
                }
            }
            get { return _Sueldo; }
        }
    }
}
