﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empleados
{
    class Program
    {
        static void Main(string[] args)
        {
            Empleado[] empleados = IngresarEmpleados();

            Console.WriteLine();

            Console.WriteLine("El sueldo más elevado es de ${0}", MayorSueldo(empleados));
            Console.WriteLine("El sueldo menos elevado es de ${0}", MenorSueldo(empleados));
            Console.WriteLine("El sueldo promedio es de ${0}", PromedioSueldo(empleados));
        }

        /// <summary>
        /// Ingreso de empleados
        /// </summary>
        /// <returns></returns>
        private static Empleado[] IngresarEmpleados()
        {
            Empleado[] resultadoEmpleados = new Empleado[3];

            for (int i = 0; i < resultadoEmpleados.Length; i++)
            {
                Empleado empleado = new Empleado();

                Console.Write("Ingrese el nombre: ");
                empleado.Nombre = Console.ReadLine();

                Console.Write("Ingrese el sueldo de {0}: ", empleado.Nombre);
                empleado.Sueldo = int.Parse(Console.ReadLine());

                resultadoEmpleados[i] = empleado;
            }

            return resultadoEmpleados;
        }

        /// <summary>
        /// Calcula el mayor sueldo de los empleados
        /// </summary>
        /// <param name="empleados"></param>
        /// <returns></returns>
        public static int MayorSueldo(Empleado[] empleados)
        {
            int mayorSueldo = 0;

            for (int i = 0; i < empleados.Length; i++)
            {
                if (mayorSueldo < empleados[i].Sueldo)
                {
                    mayorSueldo = empleados[i].Sueldo;
                }
            }

            return mayorSueldo;
        }

        /// <summary>
        /// Calcula el menor sueldo de los empleados
        /// </summary>
        /// <param name="empleados"></param>
        /// <returns></returns>
        public static int MenorSueldo(Empleado[] empleados)
        {
            int menorSueldo = int.MaxValue;

            for (int i = 0; i < empleados.Length; i++)
            {
                if (menorSueldo > empleados[i].Sueldo)
                {
                    menorSueldo = empleados[i].Sueldo;
                }
            }

            return menorSueldo;
        }

        /// <summary>
        /// Calcula el sueldo promedio de los empleados
        /// </summary>
        /// <param name="empleados"></param>
        /// <returns></returns>
        public static int PromedioSueldo(Empleado[] empleados)
        {
            int sumatoriaSueldo = 0;

            for (int i = 0; i < empleados.Length; i++)
            {
                sumatoriaSueldo += empleados[i].Sueldo;
            }

            int promedioSueldo = sumatoriaSueldo / empleados.Length;

            return promedioSueldo;
        }
    }
}
