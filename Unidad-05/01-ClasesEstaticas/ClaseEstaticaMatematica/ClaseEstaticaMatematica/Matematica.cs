﻿namespace ClaseEstaticaMatematica
{
    static class Matematica
    {
        static private string _UltimaOperacion = "No se ha ejecutado ninguna operacion";
        public static string UltimaOperacion
        {
            set
            {
                if (UltimaOperacion != null)
                {
                    _UltimaOperacion = value;
                }
            }

            get { return _UltimaOperacion; }
        }
        

        public static int Sumar(int a, int b)
        {
            UltimaOperacion = "Suma";

            return a + b;
        }

        public static int Multiplicar(int a, int b)
        {
            UltimaOperacion = "Multiplicacion";

            return a * b;
        }
    }
}
