﻿using System;

namespace ClaseEstaticaMatematica
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("(Antes de sumar) Ultima operacion = {0}", Matematica.UltimaOperacion);
            Console.WriteLine("Resultado de la suma 10 + 23 = {0}", Matematica.Sumar(10,23));
            Console.WriteLine("(Despues de sumar) Ultima operacion = {0}",Matematica.UltimaOperacion);
            Console.WriteLine("Resultado de la multiplicacion 10 * 23 = {0}", Matematica.Multiplicar(10, 23));
            Console.WriteLine("(Despues de multiplicar) Ultima operacion = {0}", Matematica.UltimaOperacion);
        }
    }
}
