﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionTeatro
{
    /// <summary>
    /// Representación en objetos de un teatro o sala en la cual se exhibe un conjunto de obras.
    /// </summary>
    public class Teatro
    {
        public Teatro()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Nombre del teatro.
        /// </summary>
        public string Nombre { set; get; }

        /// <summary>
        /// Butacas que posee físicamente el teatro.
        /// </summary>
        public Butaca[] Butacas { set; get; }

        /// <summary>
        /// Lista de obras teatrales.
        /// </summary>
        public List<Obra> Obras { set; get; }

        /// <summary>
        /// Vende a un espectador un asiento o butaca para una obra dada.
        /// </summary>
        /// <param name="nombreObraSolicitada">
        /// Nombre de la obra que desea presenciar el espectador.
        /// </param>
        /// <param name="numeroButacaSolicitada">
        /// Numero de asiento o butaca que desea obtener el espectador.
        /// </param>
        public void Vender()
        {
            throw new NotImplementedException();
        }
    }
}