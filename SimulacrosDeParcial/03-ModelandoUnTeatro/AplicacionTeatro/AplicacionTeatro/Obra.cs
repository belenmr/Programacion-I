﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionTeatro
{
    /// <summary>
    /// Representación en objetos de una obra de teatro.
    /// </summary>
    public class Obra
    {    
        /// <summary>
        /// Inicializa una obra de teatro.
        /// </summary>
        /// <param name="teatro">
        /// Teatro en el que se realizará la obra.
        /// </param>
        /// <param name="nombre">
        /// Nombre de la obra.
        /// </param>
        public Obra(Teatro teatro , string nombre)
        {
            this.Teatro = teatro;

            this.Nombre = nombre;

            //La obra inicia con cero butacas ocupadas, entiendase ocupadas como vendidas.
            this.ButacasOcupadas = new List<Butaca>();

            //La obra inicia con todas sus butacas libres.
            if (this.Teatro != null)
            {
                this.ButacasDisponibles = new List<Butaca>(this.Teatro.Butacas);       
            }

            /* Un invariante de la clase Obra, 
            * es que la cantidad de butacas ocupadas más la cantidad de butacas disponibles
            * debe ser de 100, que es la totalidad de butacas físicas en el teatro.
            */
        }

        /// <summary>
        /// Nombre de la obra.
        /// </summary>
        public string Nombre { set; get; }

        /// <summary>
        /// Teatro en el que se realizará la obra.
        /// </summary>
        public Teatro Teatro { set; get; }

        /// <summary>
        /// Butacas ocupadas o vendidas.
        /// </summary>
        public List<Butaca> ButacasOcupadas { set; get; }
        
        /// <summary>
        /// Butacas disponibles o que aún no han sido vendidas.
        /// </summary>
        public List<Butaca> ButacasDisponibles { set; get; }
    }
}