﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AplicacionTeatro
{
    /// <summary>
    /// Representación en objetos de un asiento del cual se puede ver una obra teatral.
    /// </summary>
    public class Butaca
    {
        public Butaca(Teatro teatro, int numero)
        {
            this.Teatro = teatro;
            this.Numero = numero;
        }

        /// <summary>
        /// Posición de la butaca dentro del teatro.
        /// </summary>
        /// <remarks>
        /// Identifica inequívocamente a una butaca dentro del teatro.
        /// </remarks>
        public int Numero { set; get; }

        /// <summary>
        /// Teatro al que pertenece la butaca.
        /// </summary>
        public Teatro Teatro { set; get; }
    }
}