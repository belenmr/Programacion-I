﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using AplicacionTeatro;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        Teatro _Teatro;

        [TestInitialize]
        public void TestInitialize()
        {
            _Teatro = new Teatro("Cervantes");
            _Teatro.Obras = new List<Obra>()
            {
                new Obra(_Teatro, "Las de Barranco"),
                new Obra(_Teatro, "La nona"),
                new Obra(_Teatro, "Don Quijote y Sancho Panza")
            };   
        }   

        [TestMethod]
        public void TestVender()
        {            
            //Arrange

            //Act
            _Teatro.Vender("La nona", 2);
            
            //Assert
            Assert.AreEqual(1, _Teatro.Obras[1].ButacasOcupadas.Count);
            Assert.AreEqual(99, _Teatro.Obras[1].ButacasDisponibles.Count);

            Assert.AreEqual(1, _Teatro.Obras[1].ButacasOcupadas.Where(b => b.Numero == 2).Count());
            Assert.AreEqual(0, _Teatro.Obras[1].ButacasDisponibles.Where(b => b.Numero == 2).Count());
        }

        [TestMethod]
        public void TestVenderVariasButacas()
        {
            //Arrange

            //Act
            _Teatro.Vender("La nona", 2);
            _Teatro.Vender("La nona", 55);
            _Teatro.Vender("La nona", 99);
            
            //Assert
            Assert.AreEqual(3, _Teatro.Obras[1].ButacasOcupadas.Count);
            Assert.AreEqual(97, _Teatro.Obras[1].ButacasDisponibles.Count);

            //Las butacas deben estar ocupadas (vendidas) y no disponibles
            Assert.AreEqual(3, _Teatro.Obras[1].ButacasOcupadas.Where(b => b.Numero == 2 || b.Numero == 55 || b.Numero == 99).Count());
            Assert.AreEqual(0, _Teatro.Obras[1].ButacasDisponibles.Where(b => b.Numero == 2 || b.Numero == 55 || b.Numero == 99).Count());            
        }
        
        [TestMethod]
        public void TestVenderVariasButacas_1()
        {
            //Arrange

            //Act
            _Teatro.Vender("La nona", 28);
            _Teatro.Vender("La nona", 51);
            _Teatro.Vender("La nona", 96);

            //Assert
            //Las butacas para la obra "Las de Barranco" no se deben ver afectadas por la venta de butacas para "La nona".
            Assert.AreEqual(0, _Teatro.Obras[0].ButacasOcupadas.Count);
            Assert.AreEqual(100, _Teatro.Obras[0].ButacasDisponibles.Count);

            //Las butacas para la obra "Don Quijote y Sancho Panza" no se deben ver afectadas por la venta de butacas para "La nona".
            Assert.AreEqual(0, _Teatro.Obras[2].ButacasOcupadas.Count);
            Assert.AreEqual(100, _Teatro.Obras[2].ButacasDisponibles.Count);
        }
        
        [TestMethod]
        public void TestVenderVariasButacas_2()
        {
            //Arrange

            //Act
            _Teatro.Vender("Las de Barranco", 9);
            _Teatro.Vender("Las de Barranco", 96);
            
            _Teatro.Vender("La nona", 28);
            _Teatro.Vender("La nona", 51);
            _Teatro.Vender("La nona", 96);
            
            //Assert            
            Assert.AreEqual(2, _Teatro.Obras[0].ButacasOcupadas.Count);
            Assert.AreEqual(98, _Teatro.Obras[0].ButacasDisponibles.Count);

            Assert.AreEqual(3, _Teatro.Obras[1].ButacasOcupadas.Count);
            Assert.AreEqual(97, _Teatro.Obras[1].ButacasDisponibles.Count);

            Assert.AreEqual(0, _Teatro.Obras[2].ButacasOcupadas.Count);
            Assert.AreEqual(100, _Teatro.Obras[2].ButacasDisponibles.Count);
        }
                
        [TestMethod]
        public void TestVenderVariasButacas_3()
        {
            //Arrange

            //Act
            _Teatro.Vender("Las de Barranco", 9);
            _Teatro.Vender("Las de Barranco", 96);

            _Teatro.Vender("La nona", 28);
            _Teatro.Vender("La nona", 51);
            _Teatro.Vender("La nona", 96);

            for (int i = 0; i < 100; i++ )
                _Teatro.Vender("Don Quijote y Sancho Panza", i);
            
            //Assert  
            /* Un invariante de la clase Obra, 
             * es que la cantidad de butacas ocupadas más la cantidad de butacas disponibles
             * debe ser de 100, que es la totalidad de butacas disponibles en el teatro.
             */
            Assert.AreEqual(2, _Teatro.Obras[0].ButacasOcupadas.Count);
            Assert.AreEqual(98, _Teatro.Obras[0].ButacasDisponibles.Count);

            Assert.AreEqual(3, _Teatro.Obras[1].ButacasOcupadas.Count);
            Assert.AreEqual(97, _Teatro.Obras[1].ButacasDisponibles.Count);

            Assert.AreEqual(100, _Teatro.Obras[2].ButacasOcupadas.Count);
            Assert.AreEqual(0, _Teatro.Obras[2].ButacasDisponibles.Count);
        }
        
        [TestMethod]
        [ExpectedException(typeof(System.Exception))]
        public void TestVenderObraInexistente()
        {
            //Arrange

            //Act
            //No existe una obra llamada "La super nona"
            _Teatro.Vender("La super nona", 2);
            
            //Assert            
        }

        [TestMethod]
        public void TestVenderObraInexistente_1()
        {
            //Arrange
            bool pudoVender = false;

            //Act
            try
            {
                //No existe una obra llamada "La super nona"
                _Teatro.Vender("La super nona", 2);
                pudoVender = true;
            }
            catch (Exception error)
            {
                if (!error.Message.Contains("La super nona"))
                    throw new Exception("El mensaje de error es incorrecto, ya que debe contener el nombre de la obra inexistente como parte del mismo.");
            }
            //Assert            
            Assert.AreEqual(false, pudoVender);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Exception))]
        public void TestVenderButacaQueYaFueVendida()
        {
            //Arrange
            _Teatro.Vender("La nona", 2);

            //Act
            //Se intenta vender una butaca que ya fue vendida.
            _Teatro.Vender("La nona", 2);

            //Assert            
        }

        [TestMethod]      
        public void TestVenderButacaQueYaFueVendida_1()
        {
            //Arrange
            bool pudoVender = false;
            _Teatro.Vender("La nona", 2);

            //Act
            try
            {
                //Se intenta vender una butaca que ya fue vendida.
                _Teatro.Vender("La nona", 2);
                pudoVender = true;
            }
            catch(Exception error)
            {
                if (!error.Message.Contains("2"))
                    throw new Exception("El mensaje de error es incorrecto, ya que debe contener el número de butaca que lo causo.");
            }
            //Assert            
            Assert.IsFalse(pudoVender);
        }
    }
}
