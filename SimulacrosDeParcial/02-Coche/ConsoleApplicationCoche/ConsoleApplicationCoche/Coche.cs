﻿using System;

namespace ConsoleApplicationCoche
{
    public class Coche
    {
        public Coche(int cantPuertas)
        {
            if (cantPuertas != 2 && cantPuertas != 4)
                throw new Exception("La cantidad de puertas es incorrecta.");

            this.Puertas = new Puerta[cantPuertas];
            
            //Inicializa la lista de puertas.
            for(int i=0; i < cantPuertas; i++)
            {
                this.Puertas[i] = new Puerta();                
            }

            this.Matricula = "";
        }

        public string Matricula { set; get; }

        /// <summary>
        /// Colección de puertas del coche.
        /// </summary>
        public Puerta[] Puertas { set; get; }

        private ConsoleColor _Color;
        /// <summary>
        /// Color del coche.
        /// </summary>
        /// <remarks>
        /// Tanga en cuanta que el coche debe establecerse en 
        /// su totalidad a un mismo color, en otras palabras, 
        /// si el coche se establece a color rojo, todas sus puertas 
        /// deben ser rojas.
        /// </remarks>
        public ConsoleColor Color
        {
            set 
            {
                this._Color = value;

                foreach (Puerta puerta in this.Puertas)
                {
                    puerta.Color = value;
                }
            }
            get 
            {
                return _Color;
            }
        }

        /// <summary>
        /// Abre la ventanilla de la puerta indicada.
        /// </summary>
        /// <param name="puerta">
        /// Puerta sobre la que se desea abrir la ventanilla.
        /// </param>
        public void AbrirVentanilla(Puerta puerta)
        {
            foreach (Puerta elemento in Puertas)
            {
                if (elemento == puerta)
                {
                    puerta.TieneVentanillaAbierta = true;
                }
            }
        }

        /// <summary>
        /// Abre la ventanilla de la puerta indicada.
        /// </summary>
        /// <param name="numeroDePuerta">
        /// Numero de puerta sobre la que se desea abrir la ventanilla.
        /// </param>
        public void AbrirVentanilla(int numeroDePuerta)
        {
            this.Puertas[numeroDePuerta].TieneVentanillaAbierta = true;
        }        
    }
}
