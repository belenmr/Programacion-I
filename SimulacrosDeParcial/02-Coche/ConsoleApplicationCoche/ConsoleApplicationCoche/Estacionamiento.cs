﻿using System.Collections.Generic;

namespace ConsoleApplicationCoche
{
    /// <summary>
    /// Representación en objetos de una cochera o estacionamiento.
    /// </summary>
    public class Estacionamiento
    {
        /// <summary>
        /// Inicialización de las propiedades.
        /// </summary>
        public Estacionamiento()
        {
            this.Coches = new List<Coche>();
        }

        /// <summary>
        /// Coches que se encuentran en el estacionamiento
        /// </summary>
        public List<Coche> Coches { set; get; }

        /// <summary>
        /// Ordena de menor a mayor la colección de coches que se 
        /// encuentran en el estacionamiento, por su matrícula.
        /// </summary>
        /// <remarks>
        /// Además de retornar la lista ordenada de coches, también ordena 
        /// los existentes en el estacionamiento.
        /// </remarks>
        /// <returns>
        /// Lista ordenada de coches.
        /// </returns>
        public List<Coche> OrdenarPorMatricula()
        {
            bool enroque;

            do
            {
                enroque = false;

                for (int i = 0; i < this.Coches.Count-1; i++)
                {
                    if (string.Compare(Coches[i].Matricula, Coches[i+1].Matricula) > 0)
                    // si < 0: strA < strB
                    // si = 0: strA = strB
                    // si > 0: strA > strB
                    {
                        enroque = true;
                        Coche aux = this.Coches[i];
                        this.Coches[i] = this.Coches[i + 1];
                        this.Coches[i + 1] = aux;
                    }
                }
            } while (enroque);

            return this.Coches;
        }
    }
}
