﻿using System;

namespace ConsoleApplicationCoche
{
    public class Puerta
    {
        public ConsoleColor Color { set; get; }
        public bool TieneVentanillaAbierta { set; get; }
    }
}
