﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConsoleApplicationCoche;

namespace UnitTestProjectCoche
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestColorCoche()
        {
            //Arrange
            Coche coche = new Coche(2);
            //Act
            coche.Color = ConsoleColor.Red;
            //Assert)
            Assert.AreEqual(coche.Color, ConsoleColor.Red);
        }

        [TestMethod]
        public void TestColorPuertas()
        {
            //Arrange
            Coche coche = new Coche(4);
            //Act
            coche.Color = ConsoleColor.Red;
            //Assert)
            foreach (Puerta puerta in coche.Puertas)
            {
                Assert.AreEqual(puerta.Color, ConsoleColor.Red);
            }            
        }

        [TestMethod]
        public void TestAbrirVentanilla_0()
        {
            //Arrange
            Coche coche = new Coche(4);
            //Act
            coche.AbrirVentanilla(coche.Puertas[0]);
            //Assert)           
            Assert.AreEqual(true, coche.Puertas[0].TieneVentanillaAbierta);
            Assert.AreEqual(false, coche.Puertas[1].TieneVentanillaAbierta);
            Assert.AreEqual(false, coche.Puertas[2].TieneVentanillaAbierta);
            Assert.AreEqual(false, coche.Puertas[3].TieneVentanillaAbierta);
        }

        [TestMethod]
        public void TestAbrirVentanilla_1()
        {
            //Arrange
            Coche coche = new Coche(4);
            //Act
            coche.AbrirVentanilla(0);
            //Assert)           
            Assert.AreEqual(true, coche.Puertas[0].TieneVentanillaAbierta);
            Assert.AreEqual(false, coche.Puertas[1].TieneVentanillaAbierta);
            Assert.AreEqual(false, coche.Puertas[2].TieneVentanillaAbierta);
            Assert.AreEqual(false, coche.Puertas[3].TieneVentanillaAbierta);
        }

        [TestMethod]
        public void TestOrdenarPorMatricula_0()
        {
            //Arrange
            Coche coche0 = new Coche(4);
            Coche coche1 = new Coche(4);
            Coche coche2 = new Coche(4);
            
            coche0.Matricula = "685jxt";
            coche1.Matricula = "185jxt";
            coche2.Matricula = "385jxt";

            Estacionamiento esta = new Estacionamiento();
            esta.Coches.Add(coche0);
            esta.Coches.Add(coche1);
            esta.Coches.Add(coche2);
            //Act
            esta.OrdenarPorMatricula();
            //Assert)           
            Assert.AreEqual(coche1, esta.Coches[0]);
            Assert.AreEqual(coche2, esta.Coches[1]);
            Assert.AreEqual(coche0, esta.Coches[2]);            
        }

        [TestMethod]
        public void TestOrdenarPorMatricula_1()
        {
            //Arrange
            Coche coche0 = new Coche(4);
            Coche coche1 = new Coche(4);
            Coche coche2 = new Coche(4);
            Coche coche3 = new Coche(4);

            coche0.Matricula = "zzz";
            coche1.Matricula = "aaa";
            coche2.Matricula = "jjj";
            coche3.Matricula = "bbb";

            Estacionamiento esta = new Estacionamiento();
            esta.Coches.Add(coche0);
            esta.Coches.Add(coche1);
            esta.Coches.Add(coche2);
            esta.Coches.Add(coche3);
            //Act
            List<Coche> cochesOrdenados = esta.OrdenarPorMatricula();
            //Assert)           
            Assert.AreEqual(coche1, cochesOrdenados[0]);
            Assert.AreEqual(coche3, cochesOrdenados[1]);
            Assert.AreEqual(coche2, cochesOrdenados[2]);
            Assert.AreEqual(coche0, cochesOrdenados[3]);
        }

        [TestMethod]
        public void TestOrdenarPorMatricula_2()
        {
            //Arrange
            Coche coche0 = new Coche(4);
            
            coche0.Matricula = "zzz";
            
            Estacionamiento esta = new Estacionamiento();
            esta.Coches.Add(coche0);            
            //Act
            List<Coche> cochesOrdenados = esta.OrdenarPorMatricula();
            //Assert
            Assert.AreEqual(1, cochesOrdenados.Count);            
            Assert.AreEqual(coche0, cochesOrdenados[0]);
            Assert.AreEqual("zzz", cochesOrdenados[0].Matricula);
        }
    }
}
