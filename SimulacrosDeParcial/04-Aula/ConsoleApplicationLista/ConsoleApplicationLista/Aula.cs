﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplicationLista
{
    public class Aula
    {
        public List<Alumno> Alumnos { set; get; }

        /// <summary>
        /// Busca todos los alumnos que posean un apellido similar.
        /// </summary>
        /// <remarks>
        /// Por ejemplo si el parámetro de búsqueda es : “Manu” y los alumnos son: 
        /// “Juan”, “Manuel Sadosky”, “Juan Manuel”, “Manu Ginobili” 
        /// el método debe retornar todos los alumnos excepto a: “Juan”.
        /// </remarks>
        /// <param name="parteDelApellido"></param>
        /// <returns></returns>
        public List<Alumno> BuscarApellidosParecidos(string parteDelApellido)
        {
            /***
             * Elimine este comentario e Implemente el código.
             ***/
            throw new NotImplementedException();
        }

        /// <summary>
        /// Elimina todos los alumnos que posean un apellido similar. 
        /// </summary>
        /// <remarks>
        /// Por ejemplo si el parámetro de búsqueda es : “Manu” y los alumnos son: 
        /// “Juan”, “Manuel Sadosky”, “Juan Manuel”, “Manu Ginobili” 
        /// el método debe eliminar y retornar a: 
        /// todos los alumnos excepto a: “Juan”.
        /// </remarks>
        /// <param name="parteDelApellido">
        /// Aplellido similar.
        /// </param>
        /// <returns></returns>
        public List<Alumno> ElimninarApellidosParecidos(string parteDelApellido)
        {
            /***
            * Elimine este comentario e Implemente el código.
            ***/
            throw new NotImplementedException();
        }
    }
}
