﻿using System;
using System.Collections.Generic;
using ConsoleApplicationLista;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestInitialize]
        public void TestInitialize()
        { 
        
        }

        private List<Alumno> LoadAlumnos()
        {
            List<Alumno> alumnos = new List<Alumno>();
            alumnos.Add(new Alumno() { DNI = 1, Apellido = "Sadosky" });
            alumnos.Add(new Alumno() { DNI = 1, Apellido = "Balseiro" });
            alumnos.Add(new Alumno() { DNI = 1, Apellido = "Favaloro" });
            alumnos.Add(new Alumno() { DNI = 1, Apellido = "Fava" });
            alumnos.Add(new Alumno() { DNI = 1, Apellido = "Borges" });
            return alumnos;
        }

        private Aula GetAula()
        {
            Aula aula = new Aula();
            aula.Alumnos = this.LoadAlumnos();
            return aula;
        }

        [TestMethod]
        public void TestBuscarApellidosParecidos_1()
        {
            //Arrange
            Aula aula = this.GetAula();           
            //Act
            List<Alumno> alumnos =  aula.BuscarApellidosParecidos("Fava");
            //Assert
            Assert.AreEqual(2, alumnos.Count);
        }

        [TestMethod]
        public void TestBuscarApellidosParecidos_2()
        {
            //Arrange
            Aula aula = this.GetAula();
            //Act
            List<Alumno> alumnos = aula.BuscarApellidosParecidos("Fava");
            //Assert
            Assert.IsTrue(alumnos[0].Apellido.Contains("Fava"));
            Assert.IsTrue(alumnos[1].Apellido.Contains("Fava"));
        }

        [TestMethod]
        public void TestBuscarApellidosParecidos_3()
        {
            //Arrange
            Aula aula = this.GetAula();
            //Act
            List<Alumno> alumnos = aula.BuscarApellidosParecidos("sdfasdf123123");
            //Assert
            Assert.AreEqual(0, alumnos.Count);            
        }

        [TestMethod]
        public void TestElimninarApellidosParecidos_1()
        {
            //Arrange
            Aula aula = this.GetAula();
            //Act
            List<Alumno> alumnosEliminados = aula.ElimninarApellidosParecidos("Fava");
            //Assert
            Assert.AreEqual(2, alumnosEliminados.Count);
        }

        [TestMethod]
        public void TestElimninarApellidosParecidos_2()
        {
            //Arrange
            Aula aula = this.GetAula();
            //Act
            List<Alumno> alumnosEliminados = aula.ElimninarApellidosParecidos("Fava");
            //Assert
            Assert.IsTrue(alumnosEliminados[0].Apellido.Contains("Fava"));
            Assert.IsTrue(alumnosEliminados[1].Apellido.Contains("Fava"));
        }

        [TestMethod]
        public void TestElimninarApellidosParecidos_3()
        {
            //Arrange
            Aula aula = this.GetAula();
            //Act
            List<Alumno> alumnosEliminados = aula.ElimninarApellidosParecidos("Fava");
            //Assert
            Assert.AreEqual(3, aula.Alumnos.Count);
        }

        [TestMethod]
        public void TestElimninarApellidosParecidos_4()
        {
            //Arrange
            Aula aula = this.GetAula();
            //Act
            List<Alumno> alumnosEliminados = aula.ElimninarApellidosParecidos("Fava");
            //Assert
            Assert.AreEqual("Sadosky", aula.Alumnos[0].Apellido);
            Assert.AreEqual("Balseiro", aula.Alumnos[1].Apellido);
            Assert.AreEqual("Borges", aula.Alumnos[2].Apellido);            
        }

        [TestMethod]
        public void TestElimninarApellidosParecidos_5()
        {
            //Arrange
            Aula aula = this.GetAula();
            //Act
            List<Alumno> alumnosEliminados = aula.ElimninarApellidosParecidos("dasdsFdadsv34a");
            //Assert
            Assert.AreEqual(5, aula.Alumnos.Count);          
        }

        [TestMethod]
        public void TestElimninarApellidosParecidos_6()
        {
            //Arrange
            Aula aula = this.GetAula();
            //Act
            List<Alumno> alumnosEliminados = aula.ElimninarApellidosParecidos("dasdsFdadsv34a");
            //Assert
            Assert.AreEqual(0, alumnosEliminados.Count);
        }
    }
}
