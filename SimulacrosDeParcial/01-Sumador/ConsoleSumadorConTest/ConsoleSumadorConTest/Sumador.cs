﻿using System;


namespace ConsoleSumadorConTest
{
    public class Sumador
    {
        private int _CantidadDeOperaciones;
        /// <summary>
        /// Acumula o cuenta el número total de sumas realizadas.
        /// </summary>
        /// <remarks>
        /// Por ejemplo si se invocó al método sumar 7 veces, 
        /// entonces esta propiedad tendrá como valor un 7.
        /// </remarks>
        public int CantidadDeOperaciones
        {
            get 
            {
                return _CantidadDeOperaciones;
            }
            set
            {
                /*La propiedad solo puede tomar valores comprendidos entre 0 y 100, 
                 * incluidos estos. Los valores fuera del intervalo [0, 100] 
                 * no serán válidos y el setter deberá lanzar una excepción 
                 * del tipo “Exception”.*/
                if (value >= 0 && value <= 100)
                {
                    _CantidadDeOperaciones = value;
                }
                else
                {
                    throw new Exception("Fuera de rango");
                }
            }
        }

        /// <summary>
        /// Suma algebraica de dos números enteros.
        /// </summary>
        /// <param name="a">
        /// Primer sumando.
        /// </param>
        /// <param name="b">
        /// Segundo sumando.
        /// </param>
        /// <returns></returns>
        public int Sumar(int a, int b)
        {
            int resultado;

            resultado = a + b;

            CantidadDeOperaciones++;

            return resultado;
        }

        /// <summary>
        /// Concatena dos strings.
        /// </summary>
        /// <param name="a">Primer sumando.</param>
        /// <param name="b">Segundo sumando.</param>
        /// <returns></returns>
        public string Sumar(string a, string b)
        {
            string resultado;

            resultado = a + b;

            CantidadDeOperaciones++;

            return resultado;
        }

        /// <summary>
        /// Suma dos libros.
        /// </summary>
        /// <remarks>
        /// En caso de que alguno de los parámetros sea null, 
        /// el método debe ignorarlo tratándolo como un elemento neutro 
        /// en la suma de libros. En otras palabras debe retornar un libro 
        /// cuya cantidad de hojas sea igual a la del parámetro no null.
        /// </remarks>
        /// <param name="a">
        /// Primer sumando.
        /// </param>
        /// <param name="b">
        /// Segundo sumando.
        /// </param>
        /// <returns>
        /// Da como resultado otro libro, 
        /// cuya cantidad de hojas es igual a la suma de las hojas 
        /// de los libros sumando.
        /// </returns>
        public Libro Sumar(Libro a, Libro b)
        {
            Libro resultadoLibro = new Libro();

            if (a != null && b != null)
            {
                resultadoLibro.CantHojas = a.CantHojas + b.CantHojas;
            }
            else if (a == null)
            {
                resultadoLibro.CantHojas = b.CantHojas;
            }
            else if (b == null)
            {
                resultadoLibro.CantHojas = a.CantHojas;
            }

            CantidadDeOperaciones++;

            return resultadoLibro;
        }
    }
}
