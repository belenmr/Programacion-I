﻿using System;
using ConsoleSumadorConTest;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestSumadorInt()
        {
            Sumador sumador = new Sumador();
            int r = sumador.Sumar(1, 2);
            Assert.AreEqual(3, r);
        }

        [TestMethod]
        public void TestSumadorStr()
        {
            Sumador sumador = new Sumador();
            string r = sumador.Sumar("yo no ", "fui");
            Assert.AreEqual("yo no fui", r);
        }

        [TestMethod]
        public void TestSumadorLibro()
        {
            Sumador sumador = new Sumador();
            Libro a = new Libro();
            a.CantHojas = 10;

            Libro b = new Libro();
            b.CantHojas = 1;

            Libro r = sumador.Sumar(a, b);
            Assert.AreEqual(11, r.CantHojas);
        }

        [TestMethod]
        public void TestCantidadDeOperacionesInt()
        {
            const int total = 10;
            Sumador sumador = new Sumador();
            for (int i = 0; i < total; i++)
                sumador.Sumar(1, 2);
            Assert.AreEqual(total, sumador.CantidadDeOperaciones);
        }

        [TestMethod]
        public void TestCantidadDeOperacionesStr()
        {
            const int total = 10;
            Sumador sumador = new Sumador();
            for (int i = 0; i < total; i++)
                sumador.Sumar("", "");
            Assert.AreEqual(total, sumador.CantidadDeOperaciones);
        }

        [TestMethod]
        public void TestCantidadDeOperacionesLibro()
        {
            const int total = 10;
            Sumador sumador = new Sumador();
            for (int i = 0; i < total; i++)
                sumador.Sumar(new Libro(), new Libro());
            Assert.AreEqual(total, sumador.CantidadDeOperaciones);
        }

        [TestMethod]
        public void TestSumadorLibroConParamNull_1()
        {
            Sumador sumador = new Sumador();            
            Libro r = sumador.Sumar(new Libro() { CantHojas=100}, null);
            Assert.AreEqual(100, r.CantHojas);
        }

        [TestMethod]
        public void TestSumadorLibroConParamNull_2()
        {
            Sumador sumador = new Sumador();
            Libro r = sumador.Sumar(null, new Libro() { CantHojas = 100 });
            Assert.AreEqual(100, r.CantHojas);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
        "La cantidad de operaciones no puede ser negativa.")]
        public void TestCantidadDeOperacionesNegativas()
        {
            Sumador sumador = new Sumador();
            sumador.CantidadDeOperaciones = -1;            
        }

        [TestMethod]
        [ExpectedException(typeof(Exception),
        "La cantidad de operaciones no puede ser mayor de 100.")]
        public void TestCantidadDeOperacionesMayorDe100()
        {
            Sumador sumador = new Sumador();
            sumador.CantidadDeOperaciones = 101;
        }

        [TestMethod]        
        public void TestCantidadDeOperacionesLimiteInferiorOK()
        {
            Sumador sumador = new Sumador();
            sumador.CantidadDeOperaciones = 0;
        }

        [TestMethod]
        public void TestCantidadDeOperacionesLimiteSuperiorOK()
        {
            Sumador sumador = new Sumador();
            sumador.CantidadDeOperaciones = 100;
        }

        [TestMethod]
        public void TestCantidadDeOperacionesValoresOK()
        {
            Sumador sumador = new Sumador();
            for (int i = 0; i <= 100; i++)
                sumador.CantidadDeOperaciones = i;
        }
    }
}
