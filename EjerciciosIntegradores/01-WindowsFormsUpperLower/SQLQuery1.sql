USE [CientificoDB]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Cientificos] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [Apellido] NVARCHAR (100) NOT NULL
);
