﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;


namespace DataAccessLayer
{
    public class DALCientifico : IDALCientifico
    {
        public void DeleteAll()
        {
            using (SqlConnection cnx = new SqlConnection(BaseDeDatos.CadenaDeConexion))
            {
                cnx.Open();
                const string sqlQuery = @"DELETE FROM Cientificos";

                using (SqlCommand cmd = new SqlCommand(sqlQuery,cnx))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void InsertAll()
        {
            List<string> InsertCientificos = new List<string> {
                "INSERT INTO [dbo].[Cientificos] ( [Apellido]) VALUES(N'Manuel Sadosky')",
                "INSERT INTO[dbo].[Cientificos] ( [Apellido]) VALUES(N'Balseiro')",
                "INSERT INTO[dbo].[Cientificos] ( [Apellido]) VALUES(N'César Milstein')"
            };

            foreach (string insert in InsertCientificos) {

                using (SqlConnection cnx = new SqlConnection(BaseDeDatos.CadenaDeConexion))
                {
                    cnx.Open();
                    string sqlQuery = "@" + insert;

                    using (SqlCommand cmd = new SqlCommand(sqlQuery,cnx))
                    {
                        cmd.ExecuteNonQuery();
                    }
                        
                }
            }
        }

        public DataTable SelectAll()
        {
            using (SqlConnection cnx = new SqlConnection(BaseDeDatos.CadenaDeConexion))
            {
                cnx.Open();

                string sqlQuery = "@ SELECT * FROM Cientificos";

                using (SqlCommand cmd = new SqlCommand(sqlQuery, cnx))
                {
                    
                }
            }
        }

        public void ToLower()
        {
            throw new NotImplementedException();
        }

        public void ToUpper()
        {
            throw new NotImplementedException();
        }
    }
}
