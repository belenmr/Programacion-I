﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public static class BaseDeDatos
    {
        private static string _cadenaConexion = ConfigurationManager.ConnectionStrings["cadenaConexion"].ConnectionString;
        
        public static string CadenaDeConexion
        {
            get
            {
                return _cadenaConexion;
            }
        }
        /*
        public static SqlConnection NuevaConexion()
        {
            SqlConnection cnx = new SqlConnection(CadenaDeConexion);

            cnx.Open();

            return cnx;
        }

        public static SqlCommand NuevaQuery(string sqlQuery)
        {
            SqlCommand cmd = new SqlCommand(sqlQuery, NuevaConexion());

            return cmd;
        }
        */
    }
}
